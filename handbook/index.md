The OpenCraft Handbook
======================

This handbook, along with the 
[onboarding course](https://courses.opencraft.com/courses/course-v1:OpenCraft+onboarding+course/)
and the [private documentation repository](https://gitlab.com/opencraft/documentation/private)
constitute the authoritative definition of how we run the company.

It is almost entirely public, in the spirit of openness and transparency, with the goal of
eventually only keeping the elements required to remain private for our clients in the private
repository, the rest of our organization rules being public.

The [onboarding course](https://courses.opencraft.com/courses/course-v1:OpenCraft+onboarding+course/about)
contains the information newcomers need to learn to get started, while the
handbook is meant as a reference throughout the duration of the job. Sometimes information will be
useful in both contexts - to avoid duplication, we simply link from one to the other in these cases.
As a result, when we refer to the handbook as the source of the company's "law", we also include
the onboarding course and the private repository.

OpenCraft follows a [handbook first](https://about.gitlab.com/handbook/handbook-usage/#why-handbook-first)
approach. When you want to suggest a change within OpenCraft,
[open a merge request against the handbook](https://gitlab.com/opencraft/documentation/public)
or the other two repositories mentioned above. The merge request should update the existing rules
or processes based on your suggested change. This allows people to understand, concretely, how your change
would affect these rules and processes. Once approved, the act of merging the request enacts
the change.

Also, if you ask a question which isn't answered by the current handbook, please turn the answer
you get into a merge request. This way other people wondering about the same thing in the future
won't need to ask, and can simply come here to read the answer. Thanks!
